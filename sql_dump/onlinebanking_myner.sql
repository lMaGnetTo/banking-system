-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 27, 2018 at 07:34 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `onlinebanking`
--

-- --------------------------------------------------------

--
-- Table structure for table `appointment`
--

CREATE TABLE `appointment` (
  `id` bigint(20) NOT NULL,
  `confirmed` bit(1) NOT NULL,
  `date` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `appointment`
--

INSERT INTO `appointment` (`id`, `confirmed`, `date`, `description`, `location`, `user_id`) VALUES
(1, b'1', '2017-01-25 14:01:00', 'Want to see someone', 'Helsinki', 1),
(2, b'0', '2017-01-30 15:01:00', 'Take credit', 'Helsinki', 1),
(3, b'1', '2017-02-16 15:02:00', 'Consultation', 'Helsinki', 1);

-- --------------------------------------------------------

--
-- Table structure for table `primary_account`
--

CREATE TABLE `primary_account` (
  `id` bigint(20) NOT NULL,
  `account_balance` decimal(19,2) DEFAULT NULL,
  `account_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `primary_account`
--

INSERT INTO `primary_account` (`id`, `account_balance`, `account_number`) VALUES
(1, '1700.00', 11223146),
(3, '500.00', 11223150),
(5, '0.00', 11223147);

-- --------------------------------------------------------

--
-- Table structure for table `primary_transaction`
--

CREATE TABLE `primary_transaction` (
  `id` bigint(20) NOT NULL,
  `amount` double NOT NULL,
  `available_balance` decimal(19,2) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `primary_account_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `primary_transaction`
--

INSERT INTO `primary_transaction` (`id`, `amount`, `available_balance`, `date`, `description`, `status`, `type`, `primary_account_id`) VALUES
(1, 5000, '5000.00', '2017-01-13 00:57:16', 'Deposit to Primary Account', 'Finished', 'Account', 1),
(2, 1500, '3500.00', '2017-01-13 00:57:31', 'Withdraw from Primary Account', 'Finished', 'Account', 1),
(3, 1300, '2200.00', '2017-01-13 00:58:03', 'Between account transfer from Primary to Savings', 'Finished', 'Account', 1),
(4, 500, '1700.00', '2017-01-13 00:59:08', 'Transfer to recipient Mr. Tomson', 'Finished', 'Transfer', 1),
(5, 1500, '3200.00', '2017-01-13 01:11:38', 'Deposit to Primary Account', 'Finished', 'Account', 1),
(6, 400, '2800.00', '2017-01-13 01:11:46', 'Withdraw from Primary Account', 'Finished', 'Account', 1),
(7, 2300, '2000.00', '2017-01-13 01:13:48', 'Between account transfer from Primary to Savings', 'Finished', 'Account', 1),
(8, 300, '1700.00', '2017-01-13 01:14:14', 'Transfer to recipient TaxSystem', 'Finished', 'Transfer', 1),
(9, 500, '-500.00', '2018-12-27 21:59:53', 'Between account transfer from Primary to Savings', 'Finished', 'Account', 3),
(10, 1500, '1000.00', '2018-12-27 22:12:42', 'Deposit to Primary Account', 'Finished', 'Account', 3),
(11, 300, '700.00', '2018-12-27 22:12:52', 'Withdraw from Primary Account', 'Finished', 'Account', 3),
(12, 200, '500.00', '2018-12-27 22:14:50', 'Transfer to recipient qweqwe', 'Finished', 'Transfer', 3);

-- --------------------------------------------------------

--
-- Table structure for table `recipient`
--

CREATE TABLE `recipient` (
  `id` bigint(20) NOT NULL,
  `account_number` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `recipient`
--

INSERT INTO `recipient` (`id`, `account_number`, `description`, `email`, `name`, `phone`, `user_id`) VALUES
(1, '213425635454', 'Rent payment', 'tomson@gmail.com', 'Mr. Tomson', '1112223333', 1),
(2, '453452341324', 'Gym payment', 'fitness@gmail.com', 'LtdFitness', '323245345', 1),
(3, '5465464234542', 'Tax payment 20%', 'taxes@mail.fi', 'TaxSystem', '34254353', 1),
(4, '20518561651', 'Mama', 'qweqwe@f.com', 'qweqwe', '+79798545', 3);

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `role_id` int(11) NOT NULL,
  `name` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`role_id`, `name`) VALUES
(0, 'ROLE_USER'),
(1, 'ROLE_ADMIN');

-- --------------------------------------------------------

--
-- Table structure for table `savings_account`
--

CREATE TABLE `savings_account` (
  `id` bigint(20) NOT NULL,
  `account_balance` decimal(19,2) DEFAULT NULL,
  `account_number` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `savings_account`
--

INSERT INTO `savings_account` (`id`, `account_balance`, `account_number`) VALUES
(1, '4250.00', 11223147),
(3, '500.00', 11223151),
(4, '0.00', 11223148);

-- --------------------------------------------------------

--
-- Table structure for table `savings_transaction`
--

CREATE TABLE `savings_transaction` (
  `id` bigint(20) NOT NULL,
  `amount` double NOT NULL,
  `available_balance` decimal(19,2) DEFAULT NULL,
  `date` datetime DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `status` varchar(255) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `savings_account_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `savings_transaction`
--

INSERT INTO `savings_transaction` (`id`, `amount`, `available_balance`, `date`, `description`, `status`, `type`, `savings_account_id`) VALUES
(1, 1000, '1000.00', '2017-01-13 00:57:40', 'Deposit to savings Account', 'Finished', 'Account', 1),
(2, 150, '2150.00', '2017-01-13 01:11:15', 'Withdraw from savings Account', 'Finished', 'Account', 1),
(3, 400, '1750.00', '2017-01-13 01:11:23', 'Withdraw from savings Account', 'Finished', 'Account', 1),
(4, 2000, '3750.00', '2017-01-13 01:11:30', 'Deposit to savings Account', 'Finished', 'Account', 1),
(5, 1500, '2250.00', '2017-01-13 01:13:38', 'Between account transfer from Savings to Primary', 'Finished', 'Transfer', 1),
(6, 300, '4250.00', '2017-01-13 01:14:02', 'Transfer to recipient LtdFitness', 'Finished', 'Transfer', 1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `user_id` bigint(20) NOT NULL,
  `email` varchar(255) NOT NULL,
  `enabled` bit(1) NOT NULL,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `phone` varchar(255) DEFAULT NULL,
  `username` varchar(255) DEFAULT NULL,
  `primary_account_id` bigint(20) DEFAULT NULL,
  `savings_account_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`user_id`, `email`, `enabled`, `first_name`, `last_name`, `password`, `phone`, `username`, `primary_account_id`, `savings_account_id`) VALUES
(1, 'dleskov@gmail.ru', b'1', 'Dmitry', 'Leskov', '$2a$12$DWCryEwHwbTYCegib92tk.VST.GG1i2WAqfaSwyMdxX0cl0eBeSve', '5551112345', 'User', 1, 1),
(3, 'mgtest@gmail.com', b'1', 'Refat', 'Ibraimovich', '$2a$12$hZR7pcSf0JU/OTXR3TOyuu8r6C6n.JZE8Ei47E4LZs1t0Aq1AO6oC', '+79785899654', 'Admin', 3, 3),
(4, 'mgtest123@gmail.com', b'1', 'qwe', 'qwe', '$2a$12$M557zHHB897s1FjaaAwm4OtsQKPjOBh3SMmqMkobq.agaFt/kw1MG', '+7978888888', 'qwe', 5, 4);

-- --------------------------------------------------------

--
-- Table structure for table `user_role`
--

CREATE TABLE `user_role` (
  `user_role_id` bigint(20) NOT NULL,
  `role_id` int(11) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `user_role`
--

INSERT INTO `user_role` (`user_role_id`, `role_id`, `user_id`) VALUES
(1, 0, 1),
(2, 1, 3),
(3, 0, 4);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appointment`
--
ALTER TABLE `appointment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKa8m1smlfsc8kkjn2t6wpdmysk` (`user_id`);

--
-- Indexes for table `primary_account`
--
ALTER TABLE `primary_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `primary_transaction`
--
ALTER TABLE `primary_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK643wtfdx6y0e093wlc09csehn` (`primary_account_id`);

--
-- Indexes for table `recipient`
--
ALTER TABLE `recipient`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK3041ks22uyyuuw441k5671ah9` (`user_id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indexes for table `savings_account`
--
ALTER TABLE `savings_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `savings_transaction`
--
ALTER TABLE `savings_transaction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK4bt1l2090882974glyn79q2s9` (`savings_account_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`),
  ADD UNIQUE KEY `UK_ob8kqyqqgmefl0aco34akdtpe` (`email`),
  ADD KEY `FKbj0uoj9i40dory8w4t5ojyb9n` (`primary_account_id`),
  ADD KEY `FKihums7d3g5cv9ehminfs1539e` (`savings_account_id`);

--
-- Indexes for table `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`user_role_id`),
  ADD KEY `FKa68196081fvovjhkek5m97n3y` (`role_id`),
  ADD KEY `FK859n2jvi8ivhui0rl0esws6o` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appointment`
--
ALTER TABLE `appointment`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `primary_account`
--
ALTER TABLE `primary_account`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `primary_transaction`
--
ALTER TABLE `primary_transaction`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `recipient`
--
ALTER TABLE `recipient`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `savings_account`
--
ALTER TABLE `savings_account`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `savings_transaction`
--
ALTER TABLE `savings_transaction`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `user_role`
--
ALTER TABLE `user_role`
  MODIFY `user_role_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `appointment`
--
ALTER TABLE `appointment`
  ADD CONSTRAINT `FKa8m1smlfsc8kkjn2t6wpdmysk` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `primary_transaction`
--
ALTER TABLE `primary_transaction`
  ADD CONSTRAINT `FK643wtfdx6y0e093wlc09csehn` FOREIGN KEY (`primary_account_id`) REFERENCES `primary_account` (`id`);

--
-- Constraints for table `recipient`
--
ALTER TABLE `recipient`
  ADD CONSTRAINT `FK3041ks22uyyuuw441k5671ah9` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`);

--
-- Constraints for table `savings_transaction`
--
ALTER TABLE `savings_transaction`
  ADD CONSTRAINT `FK4bt1l2090882974glyn79q2s9` FOREIGN KEY (`savings_account_id`) REFERENCES `savings_account` (`id`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `FKbj0uoj9i40dory8w4t5ojyb9n` FOREIGN KEY (`primary_account_id`) REFERENCES `primary_account` (`id`),
  ADD CONSTRAINT `FKihums7d3g5cv9ehminfs1539e` FOREIGN KEY (`savings_account_id`) REFERENCES `savings_account` (`id`);

--
-- Constraints for table `user_role`
--
ALTER TABLE `user_role`
  ADD CONSTRAINT `FK859n2jvi8ivhui0rl0esws6o` FOREIGN KEY (`user_id`) REFERENCES `user` (`user_id`),
  ADD CONSTRAINT `FKa68196081fvovjhkek5m97n3y` FOREIGN KEY (`role_id`) REFERENCES `role` (`role_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
